package cat.itb.exercici;

import java.util.concurrent.ThreadLocalRandom;

public class MainXat {
    public static void main(String[] args) {
        Xat c=new Xat();
        ThreadLocalRandom tlr = ThreadLocalRandom.current();
        Runnable u1=new UsuariPregunta(c);
        Runnable u2=new UsuariResposta(c);
        Thread t1=new Thread(u1);
        Thread t2=new Thread(u2);
        t1.start();
        t2.start();
    }
}

class Xat{
    public String question;
    public String answer;
    public boolean asked = false;

    public synchronized void pregunta(String msg){
        while(asked){
            try{
                wait();
            }catch (InterruptedException e){}
        }
        asked =true;
        question = msg;
        notify();
    }
    public synchronized void resposta(String msg){
        while (!asked){
            try {
                wait();
            }catch (InterruptedException e){}
        }
        asked = false;
        answer = msg;
        notify();
    }
}

class UsuariPregunta implements Runnable{
    private Xat xat;
    private String preguntes[] = {"tens amics", "els i agrada menjar", "saben programar"};

    @Override
    public void run() {
while (xat.question==null){
    xat.pregunta(preguntes.);
}
    }
}

class UsuariResposta implements Runnable{
    private Xat xat;
    private String respostes[]={"si","no"};
    @Override
    public void run() {

    }
}
